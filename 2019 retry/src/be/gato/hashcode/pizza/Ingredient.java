package be.gato.hashcode.pizza;


public enum Ingredient {
    TOMATO, MUSHROOM;

    public static Ingredient parse(String s) {
        return parse(s.toUpperCase().trim().charAt(0));
    }

    public static Ingredient parse(char c) {
        char upper = Character.toUpperCase(c);
        switch (upper) {
            case 'T':
                return TOMATO;
            case 'M':
                return MUSHROOM;
            default:
                throw new RuntimeException("Parse error: " + c);
        }
    }
}
