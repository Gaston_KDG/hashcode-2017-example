package be.gato.hashcode.pizza;

import be.gato.hashcode.slicing.Slice;

public class SliceClaim {
    private Slice claimant;

    public SliceClaim() {
        this.claimant = null;
    }

    public Slice getClaimant() {
        return claimant;
    }
    public boolean isClaimed() {
        return claimant!=null;
    }

    public void setClaimant(Slice claimant) {
        this.claimant = claimant;
    }
}
