package be.gato.hashcode.pizza;

import java.util.Arrays;

public class Pizza {
    private int y;
    private int x;
    private int minimumIngredients;
    private int maxCells;
    private Ingredient[][] grid;
    private SliceClaim[][] claimGrid;

    public Pizza(int y, int x, int minimumIngredients, int maxCells) {
        this.y = y;
        this.x = x;
        this.minimumIngredients = minimumIngredients;
        this.maxCells = maxCells;
        grid = new Ingredient[y][x];
        claimGrid = new SliceClaim[y][x];
    }

    public Ingredient get(int x, int y) {
        return grid[y][x];
    }

    public SliceClaim getClaim(int x, int y) {
        return claimGrid[y][x];
    }

    public void set(int x, int y, Ingredient i) {
        grid[y][x] = i;
        claimGrid[y][x] = new SliceClaim();
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public int getMinimumIngredients() {
        return minimumIngredients;
    }

    public int getMaxCells() {
        return maxCells;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (int y = 0; y < grid.length; y++) {
            for (int x = 0; x < grid[y].length; x++) {
                switch (grid[y][x]){
                    case TOMATO:
                        sb.append('T');
                        break;
                    case MUSHROOM:
                        sb.append('M');
                        break;
                }
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
