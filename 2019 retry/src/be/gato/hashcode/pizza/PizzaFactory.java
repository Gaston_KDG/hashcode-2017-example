package be.gato.hashcode.pizza;

import be.gato.io.IOFactory;
import be.gato.io.LinesReader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class PizzaFactory {
    private PizzaFactory() {
    }

    public static Pizza initPizza(Path path) {
        Pizza p;
        try (LinesReader lr = IOFactory.createLinesReader(path)) {
            String[] header = lr.readLine().split(" ");
            p = new Pizza(
                    Integer.parseInt(header[0]),
                    Integer.parseInt(header[1]),
                    Integer.parseInt(header[2]),
                    Integer.parseInt(header[3])
            );
            List<String> lines = new ArrayList<>(p.getY());
            while (lr.hasNext()) {
                lines.add(lr.readLine());
            }
            for (int i = 0; i < p.getY(); i++) {
                String line = lines.get(i);
                for (int j = 0; j < p.getX(); j++) {
                    p.set(j,i,Ingredient.parse(line.charAt(j)));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return p;
    }
}
