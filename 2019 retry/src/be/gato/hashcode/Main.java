package be.gato.hashcode;

import be.gato.hashcode.common.Coordinate;
import be.gato.hashcode.common.Performance;
import be.gato.hashcode.pizza.Pizza;
import be.gato.hashcode.slicing.PizzaChef;
import be.gato.hashcode.pizza.PizzaFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public static ExecutorService THREADPOOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    public static final String OUT_FOLDER = System.getProperty("user.home") + "/Desktop/hashcode-example/";

    public static void main(String[] args) throws InterruptedException {
        test();

        Performance.run(Main::runExample, "Example completed in");
        Performance.run(Main::runSmall, "Small completed in");
        //Performance.run(Main::runMedium, "Medium completed in");
        //Performance.run(Main::runBig, "Big completed in");
    }

    private static void test() {
        List<Coordinate> coordinates = new LinkedList<>();
        List<Integer> ints = new LinkedList<>();
        Random r = new Random();
        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 10; x++) {
                coordinates.add(new Coordinate(x, y));
                ints.add(r.nextInt(1000));
            }
        }
        Collections.sort(coordinates);
        Collections.sort(ints);
        //System.out.println(ints.toString());
        System.out.println("Test done");
    }

    private static void runBig() {
        try {
            THREADPOOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            Path outPath = initPath("big.out");
            Path path = Paths.get(ResourceLoader.BIG.toURI());
            slicePizza("Big ",outPath, path);

            THREADPOOL.shutdown();
            THREADPOOL.awaitTermination(1, TimeUnit.HOURS);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void runMedium() {
        try {
            THREADPOOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            Path outPath = initPath("medium.out");
            Path path = Paths.get(ResourceLoader.MEDIUM.toURI());
            slicePizza("Medium ",outPath, path);

            THREADPOOL.shutdown();
            THREADPOOL.awaitTermination(1, TimeUnit.HOURS);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Path initPath(String s) throws IOException {
        String outFolder = OUT_FOLDER + s;
        Path outPath = Paths.get(outFolder);
        Files.createDirectories(outPath.getParent());
        return outPath;
    }

    private static void runSmall() {
        try {
            THREADPOOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            Path outPath = initPath("small.out");
            Path path = Paths.get(ResourceLoader.SMALL.toURI());
            slicePizza("Small ",outPath, path);

            THREADPOOL.shutdown();
            THREADPOOL.awaitTermination(1, TimeUnit.HOURS);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void slicePizza(String name,Path outPath, Path path) throws InterruptedException {
        Pizza pizza = Performance.run(() -> PizzaFactory.initPizza(path), name+" Init Pizza");
        //System.out.println(pizza.toString());
        Performance.run(() -> PizzaChef.slicePizza(pizza, outPath), name+" Slice Pizza");
    }

    private static void runExample() {
        try {
            THREADPOOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            Path outPath = initPath("example.out");
            Path path = Paths.get(ResourceLoader.EXAMPLE.toURI());
            slicePizza("Example ",outPath, path);

            THREADPOOL.shutdown();
            THREADPOOL.awaitTermination(1, TimeUnit.HOURS);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
