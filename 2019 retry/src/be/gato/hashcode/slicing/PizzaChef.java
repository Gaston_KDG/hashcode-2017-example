package be.gato.hashcode.slicing;

import be.gato.hashcode.common.Coordinate;
import be.gato.hashcode.common.CoordinateOffset;
import be.gato.hashcode.pizza.Pizza;

import java.nio.file.Path;
import java.util.*;

import static be.gato.hashcode.Main.THREADPOOL;


public class PizzaChef {
    private PizzaChef() {
    }

    private static List<CoordinateOffset> generateOffsets(Pizza pizza) {
        List<CoordinateOffset> offsets = new LinkedList<>();
        int max = pizza.getMaxCells();
        for (int y = 0; y < max; y++) {
            for (int x = 0; x < max; x++) {
                if ((x + 1) * (y + 1) > max) continue;
                if ((x + 1) * (y + 1) < (pizza.getMinimumIngredients() * 2)) continue;
                offsets.add(new CoordinateOffset(x, y));
            }
        }
        return Collections.unmodifiableList(offsets);
    }

    private static Map<Coordinate, List<Slice>> generateSlices(Pizza pizza) {
        List<CoordinateOffset> offsets = generateOffsets(pizza);
        Map<Coordinate, List<Slice>> map = new HashMap<>();
        for (int y = 0; y < pizza.getY(); y++) {
            for (int x = 0; x < pizza.getX(); x++) {
                Coordinate coordinate = new Coordinate(x, y);
                List<Slice> slices = new LinkedList<>();
                for (CoordinateOffset offset : offsets) {
                    Slice slice = new Slice(coordinate, coordinate.add(offset), pizza);
                    if (slice.getArea() > pizza.getMaxCells()) continue;
                    if (slice.isValid()) slices.add(slice);
                }
                Collections.sort(slices);
                map.put(coordinate, Collections.unmodifiableList(slices));
            }
        }
        return Collections.unmodifiableMap(map);
    }

    public static void slicePizza(Pizza pizza, Path outPath) {
        Map<Coordinate, List<Slice>> slicesMap = generateSlices(pizza);
        //SliceInstance instance = new SliceInstance(pizza, slicesMap,outPath);
        AbstractSliceInstance instance = new HeavySliceInstance(pizza, slicesMap,outPath,5);
        THREADPOOL.submit(instance);
    }
}
