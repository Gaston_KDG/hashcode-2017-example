package be.gato.hashcode.slicing;

import be.gato.hashcode.common.Coordinate;
import be.gato.hashcode.pizza.Pizza;

import java.nio.file.Path;
import java.util.*;

public abstract class AbstractSliceInstance implements Runnable {
    protected final Pizza pizza;
    protected final Map<Coordinate, List<Slice>> slicesMap;
    protected final Path outPath;

    protected AbstractSliceInstance(Pizza pizza, Map<Coordinate, List<Slice>> slicesMap, Path outPath) {
        this.pizza = pizza;
        this.slicesMap = slicesMap;
        this.outPath = outPath;
        slices = new LinkedList<>();
    }

    private Queue<Slice> slices;
}
