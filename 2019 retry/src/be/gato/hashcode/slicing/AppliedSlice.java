package be.gato.hashcode.slicing;

public class AppliedSlice {
    private Slice appliedSlice;
    private int index;

    public AppliedSlice(Slice appliedSlice, int index) {
        this.appliedSlice = appliedSlice;
        this.index = index;
    }

    public Slice getAppliedSlice() {
        return appliedSlice;
    }

    public int getIndex() {
        return index;
    }
}
