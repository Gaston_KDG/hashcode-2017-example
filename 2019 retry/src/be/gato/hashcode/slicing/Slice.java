package be.gato.hashcode.slicing;

import be.gato.hashcode.common.Coordinate;
import be.gato.hashcode.common.CoordinateOffset;
import be.gato.hashcode.common.NotImplementedException;
import be.gato.hashcode.common.Rectangle;
import be.gato.hashcode.pizza.Ingredient;
import be.gato.hashcode.pizza.Pizza;

public class Slice extends Rectangle implements Comparable<Slice> {
    private Pizza pizza;

    public Slice(Coordinate topLeft, Coordinate bottomRight, Pizza pizza) {
        super(topLeft, bottomRight);
        this.pizza = pizza;
    }

    public Slice(Coordinate topLeft, CoordinateOffset offset, Pizza pizza) {
        super(topLeft, offset);
        this.pizza = pizza;
    }

    public boolean canApply() {
        for (int x = topLeft.getX(); x <= bottomRight.getX(); x++) {
            for (int y = topLeft.getY(); y <= bottomRight.getY(); y++) {
                if (pizza.getClaim(x, y).isClaimed()) return false;
            }
        }
        return true;
    }

    public void apply() {
        for (int x = topLeft.getX(); x <= bottomRight.getX(); x++) {
            for (int y = topLeft.getY(); y <= bottomRight.getY(); y++) {
                pizza.getClaim(x, y).setClaimant(this);
            }
        }
    }

    public boolean canUnApply() {
        for (int x = topLeft.getX(); x <= bottomRight.getX(); x++) {
            for (int y = topLeft.getY(); y <= bottomRight.getY(); y++) {
                if (pizza.getClaim(x, y).getClaimant() != this) return false;
            }
        }
        return true;
    }

    public void unApply() {
        for (int x = topLeft.getX(); x <= bottomRight.getX(); x++) {
            for (int y = topLeft.getY(); y <= bottomRight.getY(); y++) {
                pizza.getClaim(x, y).setClaimant(null);
            }
        }
    }

    @Override
    public int compareTo(Slice o) {
        int val = 0;
        if (val == 0) val = getTopLeft().compareTo(o.getTopLeft());
        if (val == 0) val = -Integer.compare(getArea(), o.getArea());
        if (val == 0) val = getBottomRight().compareTo(o.getBottomRight());
        return val;
    }

    public int getScore() {
        return getArea();
    }

    public boolean isValid() {
        if (getArea() > pizza.getMaxCells()) return false;

        int tomatoes = 0;
        int mushrooms = 0;

        for (int y = topLeft.getY(); y <= bottomRight.getY(); y++) {
            for (int x = topLeft.getX(); x <= bottomRight.getX(); x++) {
                try {
                    switch (pizza.get(x, y)) {
                        case TOMATO:
                            tomatoes++;
                            break;
                        case MUSHROOM:
                            mushrooms++;
                            break;
                    }
                } catch (Exception e){
                    return false;
                }
            }
        }

        if (tomatoes < pizza.getMinimumIngredients()) return false;
        if (mushrooms < pizza.getMinimumIngredients()) return false;

        return true;
    }

}
