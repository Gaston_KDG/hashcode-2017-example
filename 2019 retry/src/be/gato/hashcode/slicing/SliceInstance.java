package be.gato.hashcode.slicing;

import be.gato.hashcode.common.Coordinate;
import be.gato.hashcode.pizza.Pizza;
import be.gato.io.IOFactory;
import be.gato.io.LineAppender;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class SliceInstance extends AbstractSliceInstance {
    private final Map<Coordinate, List<Slice>> possibleSlices;
    private final Map<Coordinate, List<Slice>> untriedSlices;
    private final Map<Coordinate, List<Slice>> triedSlices;
    private final SortedSet<Coordinate> claimedCoordinates;
    private final SortedSet<Coordinate> unClaimedCoordinates;
    private final Stack<AppliedSlice> appliedSlices;

    public SliceInstance(Pizza pizza, Map<Coordinate, List<Slice>> slicesMap, Path outPath) {
        super(pizza,slicesMap,outPath);
        this.possibleSlices = copyMap();
        this.untriedSlices = new HashMap<>();
        this.triedSlices = new HashMap<>();
        appliedSlices = new Stack<>();
        claimedCoordinates = new TreeSet<>();
        unClaimedCoordinates = new TreeSet<>(slicesMap.keySet());
    }

    @Override
    public void run() {
        try {
            while (unClaimedCoordinates.size() > 0) {
                Coordinate coordinate = unClaimedCoordinates.stream().filter(this::hasPossibleSlices).findFirst().orElse(null);
                if (coordinate==null) break;

                List<Slice> possible = slicesMap.get(coordinate);
                Slice chosen = possible.stream().filter(Slice::canApply).sorted().findFirst().orElseThrow();
                apply(chosen);
            }
            outputSlices();
            long score = score();
            System.out.printf("Score: %d\n",score);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long score() {
        long l = 0;
        for (AppliedSlice appliedSlice : appliedSlices) {
            l += appliedSlice.getAppliedSlice().getArea();
        }
        return l;
    }

    private void outputSlices() throws IOException {
        try (LineAppender la = IOFactory.createLineAppender(outPath, true)) {
            la.append(appliedSlices.size());
            for (AppliedSlice appliedSlice : appliedSlices) {
                Slice s = appliedSlice.getAppliedSlice();
                la.append(String.format("%d %d %d %d", s.getTopLeft().getY(), s.getTopLeft().getX(), s.getBottomRight().getY(), s.getBottomRight().getX()));
            }
        }
        System.out.println("written to file " + outPath.toAbsolutePath().toString());
    }

    private boolean hasPossibleSlices(Coordinate c) {
        return slicesMap.get(c).stream().anyMatch(Slice::canApply);
    }

    private void clean() {
        checkSlices();
        removeEmpties();
    }

    private void apply(Slice s) {
        int idx = slicesMap.get(s.getTopLeft()).indexOf(s);
        s.apply();
        for (int x = s.getTopLeft().getX(); x <= s.getBottomRight().getX(); x++) {
            for (int y = s.getTopLeft().getY(); y <= s.getBottomRight().getY(); y++) {
                Coordinate coord = new Coordinate(x, y);
                unClaimedCoordinates.remove(coord);
                claimedCoordinates.add(coord);
            }
        }
        appliedSlices.push(new AppliedSlice(s, idx));
    }

    private AppliedSlice unApply() {
        Slice s = appliedSlices.peek().getAppliedSlice();
        for (int x = s.getTopLeft().getX(); x <= s.getBottomRight().getX(); x++) {
            for (int y = s.getTopLeft().getY(); y <= s.getBottomRight().getY(); y++) {
                Coordinate coord = new Coordinate(x, y);
                claimedCoordinates.remove(coord);
                unClaimedCoordinates.add(coord);
            }
        }
        s.unApply();
        return appliedSlices.pop();
    }

    private void checkSlices() {
        possibleSlices.entrySet().parallelStream().forEach(e -> e.getValue().removeIf(s -> !s.canApply()));
    }

    private void removeEmpties() {
        List<Coordinate> toRemove = possibleSlices.entrySet().parallelStream().filter(e -> e.getValue().size() == 0).map(Map.Entry::getKey).collect(Collectors.toList());
        toRemove.forEach(possibleSlices::remove);
    }

    private Map<Coordinate, List<Slice>> copyMap() {
        Map<Coordinate, List<Slice>> possibleSlices = new HashMap<>();
        for (Coordinate coordinate : slicesMap.keySet()) {
            possibleSlices.put(coordinate, new ArrayList<>(slicesMap.get(coordinate)));
        }
        return possibleSlices;
    }
}