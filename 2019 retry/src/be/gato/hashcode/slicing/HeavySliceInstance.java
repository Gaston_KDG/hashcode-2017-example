package be.gato.hashcode.slicing;

import be.gato.hashcode.common.Coordinate;
import be.gato.hashcode.pizza.Pizza;
import be.gato.io.IOFactory;
import be.gato.io.LineAppender;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class HeavySliceInstance extends AbstractSliceInstance {
    private int maxDepth;
    private List<Slice> possibleSlices;
    private Stack<Slice> appliedStack = new Stack<>();

    protected HeavySliceInstance(Pizza pizza, Map<Coordinate, List<Slice>> slicesMap, Path outPath, int maxDepth) {
        super(pizza, slicesMap, outPath);
        this.maxDepth = maxDepth;
        possibleSlices = slicesMap.values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Override
    public void run() {
        try {
            while (!possibleSlices.isEmpty()) {
                System.out.printf("\r%d possible slices",possibleSlices.size());
                Queue<Slice> toApply = findBestFuture();
                applyAll(toApply);
                System.out.printf("\rApplied %d of %d possible slices",appliedStack.size(),possibleSlices.size());
            }
            System.out.println();
            outputSlices();
            long score = score();
            System.out.printf("Score: %d\n", score);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void outputSlices() throws IOException {
        try (LineAppender la = IOFactory.createLineAppender(outPath, true)) {
            la.append(appliedStack.size());
            for (Slice s : appliedStack) {
                la.append(String.format("%d %d %d %d", s.getTopLeft().getY(), s.getTopLeft().getX(), s.getBottomRight().getY(), s.getBottomRight().getX()));
            }
        }
        System.out.println("written to file " + outPath.toAbsolutePath().toString());
    }

    private void applyAll(Queue<Slice> toApply) {
        for (Slice slice : toApply) {
            appliedStack.push(slice);
        }
        appliedStack.forEach(as->as.getCoordinates().forEach(coordinate -> possibleSlices.removeIf(s -> s.contains(coordinate))));
    }

    private Queue<Slice> findBestFuture() {
        PizzaFuture future = possibleSlices.parallelStream().map(this::findFuture).max(Comparator.comparing(pf -> pf.score)).orElseThrow();
        Queue<Slice> queue = new LinkedList<>();
        for (Slice slice : future.sliceStack) {
            queue.offer(slice);
        }
        return queue;
    }

    private PizzaFuture findFuture(Slice s) {
        Stack<Slice> stack = new Stack<>();
        stack.add(s);

        var future = findFuture(stack, new LinkedList<>(possibleSlices), 0);
        return future;
    }

    private PizzaFuture findFuture(Stack<Slice> sliceStack, List<Slice> slices, int ct) {
        boolean lastRecursion = ct == maxDepth;

        List<Slice> availableSlices = new LinkedList<>(slices);
        for (Coordinate coordinate : sliceStack.peek().getCoordinates()) {
            availableSlices.removeIf(s -> s.contains(coordinate));
        }

        lastRecursion = lastRecursion || availableSlices.size() == 0;

        if (lastRecursion) {
            PizzaFuture future = new PizzaFuture();
            future.availableSlices = availableSlices;
            future.sliceStack = sliceStack;
            future.score = scoreFuture(sliceStack, availableSlices);
            return future;
        } else {
            PizzaFuture bestFuture = new PizzaFuture();
            bestFuture.score = BigDecimal.ZERO;

            for (Slice availableSlice : availableSlices) {
                Stack<Slice> newSlices = new Stack<>();
                newSlices.addAll(sliceStack);
                newSlices.push(availableSlice);
                PizzaFuture future = findFuture(newSlices, availableSlices, ct+1);
                if (future.score.compareTo(bestFuture.score) > 0) {
                    bestFuture = future;
                }
            }

            return bestFuture;
        }
    }

    private long score() {
        long l = 0;
        for (Slice appliedSlice : appliedStack) {
            l += appliedSlice.getScore();
        }
        return l;
    }

    private BigDecimal scoreFuture(Stack<Slice> sliceStack, List<Slice> availableSlices) {
        BigDecimal total = BigDecimal.valueOf(availableSlices.stream().mapToInt(Slice::getScore).sum());

        if (availableSlices.size()>0){
            BigDecimal div = BigDecimal.valueOf(availableSlices.size());
            total = total.divide(div,6, RoundingMode.HALF_UP);
        }

        BigDecimal bd = new BigDecimal(0)
                .add(BigDecimal.valueOf(appliedStack.stream().mapToInt(Slice::getScore).sum()))
                .add(BigDecimal.valueOf(sliceStack.stream().mapToInt(Slice::getScore).sum()))
                .add(total);
        return bd;
    }

    private class PizzaFuture {
        private Stack<Slice> sliceStack;
        private BigDecimal score;
        private List<Slice> availableSlices;

        public PizzaFuture() {
            this.sliceStack = new Stack<>();
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", PizzaFuture.class.getSimpleName() + "[", "]")
                    .add("score=" + score)
                    .toString();
        }
    }
}
