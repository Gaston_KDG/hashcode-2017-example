package be.gato.hashcode.common;

public class Flag {
    private boolean raised;

    public Flag() {
        this(false);
    }

    public Flag(boolean raised) {
        this.raised = raised;
    }

    public void raise() {
        raised = true;
    }


    public void setRaised(boolean raised) {
        this.raised = raised;
    }
    public void toggle() {
        raised = !raised;
    }

    public boolean isRaised() {
        return raised;
    }
}
