package be.gato.hashcode.common;

import java.io.Serializable;

public class Coordinate implements Serializable, Comparable<Coordinate> {
    private int x;
    private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int compareTo(Coordinate o) {
        int result = 0;
        if (result == 0) result = Integer.compare(y, o.y);
        if (result == 0) result = Integer.compare(x, o.x);
        return result;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Coordinate addX(int x) {
        return new Coordinate(this.x + x, this.y);
    }

    public Coordinate addY(int y) {
        return new Coordinate(this.x, this.y + y);
    }

    public Coordinate add(Coordinate c) {
        return new Coordinate(this.x + c.x, this.y + c.y);
    }

    public boolean borders(Coordinate c) {
        return c.x >= x - 1 &&
                c.x <= x + 1 &&
                c.y <= y + 1 &&
                c.y >= y - 1 &&
                !equals(c);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordinate)) return false;

        Coordinate coordinate = (Coordinate) o;

        if (x != coordinate.x) return false;
        return y == coordinate.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(x).append(",").append(y);
        return sb.toString();
    }
}
