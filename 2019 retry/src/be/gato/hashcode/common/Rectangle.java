package be.gato.hashcode.common;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Rectangle {
    protected Coordinate topLeft;
    protected Coordinate bottomRight;

    public Rectangle(Coordinate topLeft, Coordinate bottomRight) {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public Rectangle(Coordinate topLeft, CoordinateOffset offset) {
        this(topLeft, topLeft.add(offset));
    }

    public int getArea() {
        int h = bottomRight.getY() - topLeft.getY() + 1;
        int w = bottomRight.getX() - topLeft.getX() + 1;
        return w * h;
    }

    public Coordinate getBottomRight() {
        return bottomRight;
    }

    public Coordinate getTopLeft() {
        return topLeft;
    }

    public List<Coordinate> getCoordinates() {
        List<Coordinate> coordinates = new LinkedList<>();
        for (int y = topLeft.getY(); y <= bottomRight.getY(); y++) {
            for (int x = topLeft.getX(); x <= bottomRight.getX(); x++) {
                coordinates.add(new Coordinate(x, y));
            }
        }
        return coordinates;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(topLeft).append(" ").append(bottomRight);
        return sb.toString();
    }

    public boolean contains(Coordinate coordinate) {
        boolean containsX = coordinate.getX() >= topLeft.getX() && coordinate.getX() <= bottomRight.getX();
        boolean containsY = coordinate.getY() >= topLeft.getY() && coordinate.getY() <= bottomRight.getY();
        return containsX && containsY;
    }
}
