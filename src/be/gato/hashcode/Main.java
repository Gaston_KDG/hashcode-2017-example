package be.gato.hashcode;

import be.gato.hashcode.pizza.PizzaRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 2017
 * 1st session 0:44:00
 * 2th session 0:56:00
 * 3th session 0:30:00
 * 4th session 0:40:00 --> RUN 1: complete run, Example, invalid output
 * 4th session 0:12:00 --> RUN 2: complete run, Example, invalid output
 * 4th session 0:12:00 --> RUN 3: complete run, Example, valid output
 * 4th session 0:2:00 --> RUN 4: complete run, SMALL, invalid output
 * 4th session 0:20:00 --> RUN 5: complete run, SMALL, invalid output
 *
 * 2018
 * Major refactor
 */
public class Main {
    public static final Random R = new Random();
    private static final String DIRECTORY = "hashcode";
    //private static final String[] FILES = {"example","small","medium","big"};
    //private static final String[] FILES = {"example"};
    private static final String[] FILES = {"small"};
    //private static final String[] FILES = {"medium"};
    //private static final String[] FILES = {"big"};
    public static final String INPUT_EXTENSION = ".in";
    public static final String OUTPUT_EXTENSION = ".out";
    private final static Map<String,String> threadReports = new HashMap<>();
    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newFixedThreadPool(FILES.length);
        for (int i = 0; i < FILES.length; i++) {
            threadPool.execute(new PizzaRunner(DIRECTORY, FILES[i]));
        }
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void writeOutput(String thread,String s){
        threadReports.put(thread,s);
        /*
        for (int i = 0; i < 20; i++) {
            System.out.println();
        }
        */
        System.out.println("#######################################################");
        for (String key : threadReports.keySet()) {
            System.out.printf("%-10s: %s\n",key,threadReports.get(key));
        }
    }
}
