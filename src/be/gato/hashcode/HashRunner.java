package be.gato.hashcode;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 21/01/2018
 */
public abstract class HashRunner implements Runnable{
    protected final String directory;
    protected final String file;

    public HashRunner(String directory, String file) {
        this.directory = directory;
        this.file = file;
    }

    @Override
    public abstract void run();
}
