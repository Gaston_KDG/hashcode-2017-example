package be.gato.hashcode.gui;

import be.gato.hashcode.pizza.pojo.Pizza;
import be.gato.hashcode.pizza.pojo.Slice;

import javax.swing.*;
import java.awt.*;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode.gui
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 26/02/2018
 */
public class CellPanel extends JPanel {
    private static final Color BACKGROUND_COLOR = new Color(255, 255, 255);
    private static final Color TOMATO_COLOR = new Color(255, 0, 0);
    private static final Color MUSHROOM_COLOR = new Color(0, 255, 0);
    private Slice s;
    private Pizza p;
    private int row, col;

    public CellPanel(Pizza p, int row, int col) {
        this.p = p;
        this.row = row;
        this.col = col;
        s = null;
        updateColor();
    }

    private void updateColor() {
        if (s == null) {
            switch (p.pizza.get(row).get(col).TYPE) {
                case TOMATO:
                    setBackground(TOMATO_COLOR);
                    break;
                case MUSHROOM:
                    setBackground(MUSHROOM_COLOR);
                    break;
                default:
                    setBackground(BACKGROUND_COLOR);
            }
        } else {
            setBackground(s.getColor());
        }
        repaint();
    }

    public Slice getS() {
        return s;
    }

    public void setSlice(Slice s) {
        this.s = s;
        updateColor();
    }
}
