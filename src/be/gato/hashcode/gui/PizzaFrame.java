package be.gato.hashcode.gui;

import be.gato.hashcode.pizza.pojo.Coordinate;
import be.gato.hashcode.pizza.pojo.Pizza;
import be.gato.hashcode.pizza.pojo.Slice;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode.gui
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 26/02/2018
 */
public class PizzaFrame extends JFrame {
    private static PizzaFrame instance;
    private final static int GRID_GAP = 0;
    private static  Map<Coordinate, Slice> slices;
    private static  Map<Coordinate, CellPanel> cellPanels;
    private static  int ROWS;
    private static  int COLS;
    private static long loopCount;
    private static boolean updateRequested;
    private static boolean stop;
    private static boolean drawFinalCalled = false;
    private static JLabel lblLoopCount;
    private JButton btnExit;
    private JPanel pnlNorth, pnlMiddle;
    private Pizza pizza;

    public static void stop() {
    }

    public static void startGui(int rows, int cols, Map<Coordinate, Slice> slices, Pizza pizza) {
        Thread t = new Thread(new PizzaGuiRunnable(rows, cols, slices, pizza));
        t.setName("GUI THREAD");
        t.setPriority(Thread.NORM_PRIORITY + 2);
        t.start();
    }

    private PizzaFrame(int rows, int cols, Map<Coordinate, Slice> slices, Pizza pizza) throws HeadlessException {
        super("Pizza View");
        //this.slices = slices;
        //ROWS = rows;
        //COLS = cols;
        this.pizza = pizza;
        loopCount = 0;
        stop = false;
        updateRequested = true;
        //cellPanels = new HashMap<>();

        createComponents();
        applyLayout();
        setListeners();
        showFrame();
    }

    private void applyLayout() {
        setMaximumSize(new Dimension(1000, 1000));
        setPreferredSize(new Dimension(1500, 900));
        setMinimumSize(new Dimension(500, 500));
        pnlNorth.setLayout(new FlowLayout());
        pnlMiddle.setLayout(new GridLayout(ROWS, COLS, GRID_GAP, GRID_GAP));
        setLayout(new BorderLayout());

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                Coordinate coord = new Coordinate(i, j);
                pnlMiddle.add(cellPanels.get(coord));
            }
        }

        pnlNorth.add(lblLoopCount);
        pnlNorth.add(btnExit);

        add(pnlNorth, BorderLayout.NORTH);
        add(pnlMiddle, BorderLayout.CENTER);
    }

    private void createComponents() {
        lblLoopCount = new JLabel("Loops: " + loopCount);
        btnExit = new JButton("Exit");
        pnlMiddle = new JPanel();
        pnlNorth = new JPanel();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                Coordinate coord = new Coordinate(i, j);
                cellPanels.put(coord, new CellPanel(pizza, i, j));
            }
        }
    }

    private void setListeners() {
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stop = true;
            }
        });
    }

    private void showFrame() {
        setVisible(true);
    }

    public static void drawFinal() {
        drawFinalCalled = true;
    }

    public static void updatePizza(long loopCounter) {
        synchronized (instance) {
            loopCount = loopCounter;
            if (!updateRequested) {
                updateRequested = true;
            }
        }
    }

    private static class PizzaGuiRunnable implements Runnable {

        public PizzaGuiRunnable(int rows, int cols, Map<Coordinate, Slice> slices, Pizza pizza) {
            PizzaFrame.slices = slices;
            ROWS = rows;
            COLS = cols;
            cellPanels = new HashMap<>();
            instance = new PizzaFrame(rows, cols, slices, pizza);
        }

        @Override
        public void run() {
            while (!stop) {
                boolean upReq;
                boolean drwFnl;

                synchronized (this) {
                    upReq = updateRequested;
                    drwFnl = drawFinalCalled;
                }

                if (upReq) {
                    upReq = false;
                    lblLoopCount.setText("Loop: " + loopCount);

                    synchronized (slices) {
                        for (int i = 0; i < ROWS; i++) {
                            for (int j = 0; j < COLS; j++) {
                                Coordinate coord = new Coordinate(i, j);
                                CellPanel cellPanel = cellPanels.get(coord);
                                Slice slice = slices.get(coord);
                                cellPanel.setSlice(slice);
                            }
                        }
                    }
                    //pack();
                    instance.repaint();
                }
                if (drwFnl) {
                    stop = true;
                    upReq = false;
                    lblLoopCount.setText("Loop: " + loopCount);

                    synchronized (slices) {
                        for (int i = 0; i < ROWS; i++) {
                            for (int j = 0; j < COLS; j++) {
                                Coordinate coord = new Coordinate(i, j);
                                CellPanel cellPanel = cellPanels.get(coord);
                                Slice slice = slices.get(coord);
                                cellPanel.setSlice(slice);
                            }
                        }
                    }
                    //pack();
                    instance.repaint();
                }


                synchronized (this) {
                    updateRequested = upReq;
                    drawFinalCalled = drwFnl;
                }
            }
            System.exit(1);
        }
    }
}
