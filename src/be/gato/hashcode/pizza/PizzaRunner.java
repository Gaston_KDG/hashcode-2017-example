package be.gato.hashcode.pizza;

import be.gato.hashcode.HashRunner;
import be.gato.hashcode.gui.PizzaFrame;
import be.gato.hashcode.pizza.exception.PizzaException;
import be.gato.hashcode.pizza.pojo.CellType;
import be.gato.hashcode.pizza.pojo.Pizza;
import be.gato.hashcode.pizza.slicers.BruteSlicer;
import be.gato.hashcode.pizza.slicers.Slicer;
import be.gato.io.IOFactory;
import be.gato.io.LineAppender;
import be.gato.io.LinesReader;

import java.io.IOException;
import java.util.List;

import static be.gato.hashcode.Main.INPUT_EXTENSION;
import static be.gato.hashcode.Main.OUTPUT_EXTENSION;
import static be.gato.hashcode.Main.writeOutput;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 21/01/2018
 */
public class PizzaRunner extends HashRunner {

    //private final String directory, file;

    public PizzaRunner(String directory, String file) {
        super(directory,file);
    }

    @Override
    public void run() {
        try {
            long t1,t2;
            t1 = System.currentTimeMillis();
            LinesReader lr = IOFactory.createLinesReader(directory, file + INPUT_EXTENSION);
            int r, c, l, h;
            String[] lead = lr.readLine().split(" ");
            r = Integer.parseInt(lead[0]);// Rows
            c = Integer.parseInt(lead[1]);// Cols
            l = Integer.parseInt(lead[2]);// Min of each ingredient
            h = Integer.parseInt(lead[3]);// Max total cells
            threadWrite("Header read");
            char[][] pizzarray = new char[r][c];
            for (int i = 0; i < r; i++) {
                char[] row = lr.readLine().toCharArray();
                if (row.length != c) {
                    StringBuilder sb = new StringBuilder();
                    for (char ch : row) {
                        if (CellType.getType(ch) != null) {
                            sb.append(ch);
                        }
                    }
                    row = sb.toString().toCharArray();
                }
                pizzarray[i] = row;
            }
            lr.close();
            threadWrite("Pizzarray read");
            Pizza pizza = new Pizza(pizzarray);
            threadWrite("Slicing");
            Slicer slicer;
            //slicer = new SplitSlicer(pizza, l, h);
            slicer = new BruteSlicer(pizza, l, h);
            slicer.slice();
            threadWrite("Pizza sliced");
            List<String> out = slicer.outputData();
            threadWrite("Writing output data");
            LineAppender la = IOFactory.createLineAppender(directory, file+OUTPUT_EXTENSION, true);
            la.appendLines(out);
            la.close();
            t2 = System.currentTimeMillis();
            threadWrite("processing of file " + file+INPUT_EXTENSION+ " finished in " +(t2-t1)+ "ms");
        } catch (IOException | PizzaException e) {
            threadWrite("Failed processing of file "+file+INPUT_EXTENSION+" "+ e.getMessage());
        }
    }

    private void threadWrite(String s){
        writeOutput(file,s);
    }
}
