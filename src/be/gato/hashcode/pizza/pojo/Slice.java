package be.gato.hashcode.pizza.pojo;

import be.gato.hashcode.pizza.exception.PizzaException;

import java.awt.*;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

import static be.gato.hashcode.Main.R;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode.pizza.pojo
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 22/01/2018
 */
public class Slice implements Comparable<Slice> {
    private Pizza PIZZA;
    private int[] COORDS; //(r1,c1),(r2,c2)
    private int ROWS, COLS, SIZE;
    private final Color color;

    private Slice() {
        color = new Color(R.nextInt(128),R.nextInt(128),R.nextInt(128));
    }

    public Slice(Pizza PIZZA, int[] COORDS) {
        this();
        this.PIZZA = PIZZA;
        this.COORDS = COORDS;
        ROWS = COORDS[2] - COORDS[0] + 1;
        COLS = COORDS[3] - COORDS[1] + 1;
        SIZE = ROWS*COLS;
    }

    public Slice(Pizza PIZZA, Coordinate coordinate,CoordinateOffset coordinateOffset) throws PizzaException {
        this();
        this.PIZZA = PIZZA;
        this.COORDS = new int[]{coordinate.getRow(),coordinate.getCol(),coordinate.getRow()+coordinateOffset.getRowOffset(),coordinate.getCol()+coordinateOffset.getColOffset()};
        ROWS = COORDS[2] - COORDS[0] + 1;
        COLS = COORDS[3] - COORDS[1] + 1;
        SIZE = ROWS*COLS;
        if (COORDS[2] >= PIZZA.rows || COORDS[3] >= PIZZA.cols) throw new PizzaException("Slice out of bounds");
    }

    @Override
    public int compareTo(Slice o) {
        return Integer.compare(o.SIZE,SIZE);
    }

    public Pizza getPIZZA() {
        return PIZZA;
    }

    public int[] getCOORDS() {
        return COORDS;
    }

    public int getROWS() {
        return ROWS;
    }

    public int getCOLS() {
        return COLS;
    }

    public int getSIZE() {
        return SIZE;
    }

    @Override
    public String toString() {
        return String.format("%d %d %d %d",COORDS[0],COORDS[1],COORDS[2],COORDS[3]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slice slice = (Slice) o;
        return Objects.equals(getPIZZA(), slice.getPIZZA()) &&
                Arrays.equals(getCOORDS(), slice.getCOORDS());
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(getPIZZA());
        result = 31 * result + Arrays.hashCode(getCOORDS());
        return result;
    }

    public Color getColor() {
        return color;
    }
}
