package be.gato.hashcode.pizza.pojo;

import java.util.Objects;

/**
 * @author Gaston
 * @version 1.0
 * @since 4/02/2017
 */
public class Cell {
    public final CellType TYPE;
    public final int ROW;
    public final int COL;

    public Cell(CellType type, int row, int col) {
        this.TYPE = type;
        this.ROW = row;
        this.COL = col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return ROW == cell.ROW &&
                COL == cell.COL &&
                TYPE == cell.TYPE;
    }

    @Override
    public int hashCode() {

        return Objects.hash(TYPE, ROW, COL);
    }
}
