package be.gato.hashcode.pizza.pojo;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode.pizza.pojo
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 29/01/2018
 */
public class CoordinateOffset {
    private int rowOffset,colOffset;

    public CoordinateOffset(int rowOffset, int colOffset) {
        this.rowOffset = rowOffset;
        this.colOffset = colOffset;
    }

    public int getRowOffset() {
        return rowOffset;
    }

    public int getColOffset() {
        return colOffset;
    }
}
