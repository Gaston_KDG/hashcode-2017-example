package be.gato.hashcode.pizza.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Gaston
 * @version 1.0
 * @since 4/02/2017
 */
public class Pizza {
    public final int rows;
    public final int cols;
    public final long size;
    public final List<List<Cell>> pizza;

    public Pizza(char[][] pizzarray) {
        rows = pizzarray.length;
        cols = pizzarray[0].length;
        size = ((long)rows) * ((long)cols);
        pizza = initPizza(pizzarray);
    }

    private List<List<Cell>> initPizza(char[][] pizzarray) {
        List<List<Cell>> nPizza = new ArrayList<>();
        for (int i = 0; i < pizzarray.length; i++) {
            char[] row = pizzarray[i];
            ArrayList<Cell> nRow = new ArrayList<>(row.length);

            for (int j = 0; j < row.length; j++) {
                char c = row[j];
                nRow.add(new Cell(CellType.getType(c),i,j));
            }
            nPizza.add(nRow);
        }
        return nPizza;
    }

    public CellType getType(int r,int c){
        return pizza.get(r).get(c).TYPE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza1 = (Pizza) o;
        return rows == pizza1.rows &&
                cols == pizza1.cols &&
                Objects.equals(pizza, pizza1.pizza);
    }

    @Override
    public int hashCode() {

        return Objects.hash(rows, cols, pizza);
    }
}
