package be.gato.hashcode.pizza.pojo;

import be.gato.hashcode.pizza.slicers.Slicer;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode.pizza.pojo
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 23/01/2018
 */
public class Coordinate implements Comparable<Coordinate> {
    private final int row, col;

    public Coordinate(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public int compareTo(Coordinate o) {
        int result = Integer.compare(row,o.row);
        //int result = Integer.compare(o.row,row);
        //int result = Integer.compare(col,o.col);
        //int result = Integer.compare(o.col,col);
        if (result == 0){
            //result = Integer.compare(o.row,row);
            //result = Integer.compare(row,o.row);
            //result = Integer.compare(col,o.col);
            result = Integer.compare(o.col,col);
        }
        return result;
    }

    public Coordinate down(int i) {
        return new Coordinate(row + i, col);
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    @Override
    public int hashCode() {

        return Objects.hash(getRow(), getCol());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return getRow() == that.getRow() &&
                getCol() == that.getCol();
    }

    public Coordinate left(int i) {
        return right(-i);
    }

    public Coordinate right(int i) {
        return new Coordinate(row, col + i);
    }

    public Coordinate up(int i) {
        return down(-i);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append('[').append(col).append(',').append(row).append(']');
        return sb.toString();
    }

    public static class CoordinateComparator implements Comparator<Coordinate>{
        private Slicer slicer;
        private Map<Coordinate, List<Slice>> possibleSlices;

        public CoordinateComparator(Slicer slicer, Map<Coordinate, List<Slice>> possibleSlices) {
            this.slicer = slicer;
            this.possibleSlices = possibleSlices;
        }

        @Override
        public int compare(Coordinate o1, Coordinate o2) {
            int count1 = slicer.filterValidSlices(possibleSlices.get(o1)).size();
            int count2 = slicer.filterValidSlices(possibleSlices.get(o2)).size();
            return Integer.compare(count1,count2);
        }
    }
}
