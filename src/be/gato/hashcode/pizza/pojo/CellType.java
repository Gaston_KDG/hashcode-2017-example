package be.gato.hashcode.pizza.pojo;

/**
 * @author Gaston
 * @version 1.0
 * @since 4/02/2017
 */
public enum CellType {
    TOMATO,MUSHROOM;
    public static CellType getType(char c){
        char ch = Character.toLowerCase(c);
        switch (ch){
            case 't':return TOMATO;
            case 'm':return MUSHROOM;
            default:return null;
        }
    }
}
