package be.gato.hashcode.pizza.slicers;

import be.gato.hashcode.gui.PizzaFrame;
import be.gato.hashcode.pizza.exception.PizzaException;
import be.gato.hashcode.pizza.pojo.Coordinate;
import be.gato.hashcode.pizza.pojo.CoordinateOffset;
import be.gato.hashcode.pizza.pojo.Pizza;
import be.gato.hashcode.pizza.pojo.Slice;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode.pizza.slicers
 * <p>
 * Example: TESTED OK
 * Small  : TESTED NFG
 * Medium :
 * Big    :
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 23/01/2018
 */
public class BruteSlicer extends Slicer {
    private static long coordsGenerated = 0;
    private static long loopCounter = 0;
    private final Map<Coordinate, List<Slice>> possibleSlices;
    private final Map<Coordinate, Slice> slices;
    private final ExecutorService threadPoolCached;
    //private PizzaFrame pizzaFrame;
    private int nextStepIndex = 0;
    private List<Step> steps;
    private int filledCells = 0;
    private List<CoordinateOffset> coordOffsets;
    private final Coordinate.CoordinateComparator coordinateComparator;


    public BruteSlicer(Pizza pizza, int l, int h) throws PizzaException {
        super(pizza, l, h);
        possibleSlices = new ConcurrentHashMap<>();
        slices = new HashMap<>();
        threadPoolCached = Executors.newFixedThreadPool(6);
        steps = new LinkedList<>();
        coordOffsets = new ArrayList<>();
        coordinateComparator = new Coordinate.CoordinateComparator(this,possibleSlices);
    }

    private void applyProblemSlices() throws PizzaException {
        boolean rerun;
        Map<Coordinate, List<Slice>> appliedSlices = new HashMap<>();
        for (int i = 0; i < pizza.rows; i++) {
            for (int j = 0; j < pizza.cols; j++) {
                Coordinate coord = new Coordinate(i, j);
                appliedSlices.put(coord, new LinkedList<>());
            }
        }
        for (List<Slice> sliceList : possibleSlices.values()) {
            for (Slice slice : sliceList) {
                for (int i = slice.getCOORDS()[0]; i <= slice.getCOORDS()[2]; i++) {
                    for (int j = slice.getCOORDS()[1]; j <= slice.getCOORDS()[3]; j++) {
                        Coordinate coordinate = new Coordinate(i, j);
                        appliedSlices.get(coordinate).add(slice);
                    }
                }
            }
        }

        do {
            rerun = false;
            for (Coordinate coordinate : appliedSlices.keySet()) {
                if (slices.get(coordinate) != null) continue;

                if (appliedSlices.get(coordinate).size() == 1) {

                    Slice slice = appliedSlices.get(coordinate).get(0);
                    Step step = new Step(nextStepIndex++, coordinate);
                    applySlice(slice);
                    step.nextStepCoordsFound = generateCorners(slices);
                    step.nextStepCoordsSaturated = new LinkedList<>();
                    step.sliceUsed = slice;
                    steps.add(step);


                    //for all coordinates in this slice
                    for (int i = slice.getCOORDS()[0]; i <= slice.getCOORDS()[2]; i++) {
                        for (int j = slice.getCOORDS()[1]; j <= slice.getCOORDS()[3]; j++) {
                            Coordinate coord = new Coordinate(i, j);
                            //find slices originating from this coord
                            List<Slice> sliceList = possibleSlices.get(coord);
                            //for all coordinates in each of those slices
                            for (Slice s : sliceList) {
                                for (int k = s.getCOORDS()[0]; k <= s.getCOORDS()[2]; k++) {
                                    for (int l = s.getCOORDS()[1]; l <= s.getCOORDS()[3]; l++) {
                                        Coordinate coord2 = new Coordinate(k, l);
                                        //remove the slice from applied list
                                        appliedSlices.get(coord2).remove(s);
                                    }
                                }
                            }
                        }
                    }
                    rerun = true;
                } else if (appliedSlices.get(coordinate).size() == 0) {
                    //holy fuck this is bad
                    throw new PizzaException("No slices applicable to coordinate " + coordinate.toString());
                }
            }
        } while (rerun);
        //displayPizzaSlices();
    }

    private void applySlice(Slice slice) throws PizzaException {
        boolean back = false;
        boolean alreadyApplied = false;
        synchronized (slices) {
            for (int i = slice.getCOORDS()[0]; i <= slice.getCOORDS()[2]; i++) {
                for (int j = slice.getCOORDS()[1]; j <= slice.getCOORDS()[3]; j++) {
                    Coordinate coordinate = new Coordinate(i, j);
                    Slice s = slices.get(coordinate);
                    if (s != null) {
                        //there already is a slice
                        if (!s.equals(slice)) {//check if that slice is the one we're trying to apply
                            back = true;
                            //throw new PizzaException("Tried to apply slice to claimed coord");
                        } else {
                            alreadyApplied = true;
                            break;
                        }
                    } else {
                        slices.put(coordinate, slice);
                        filledCells++;
                    }
                }
            }
        }
        //if (!alreadyApplied) filledCells += slice.getSIZE();
        if (back) {
            //displayPizzaSlices();
            undoSlice(slice);
            //stepBack();
        }
    }

    private void displayPizzaSlices() {
        StringBuilder sb = new StringBuilder();
        StringBuilder sep = new StringBuilder();
        List<Slice> sliceList = new ArrayList<>();
        Map<Slice, Integer> sliceIndexes = new HashMap<>();

        for (Step step : steps) {
            if (step.sliceUsed != null) {
                sliceIndexes.put(step.sliceUsed, step.index);
            }
        }

        for (int i = 0; i < pizza.rows; i++) {
            for (int j = 0; j < pizza.cols; j++) {
                Coordinate coordinate = new Coordinate(i, j);
                Slice s = slices.get(coordinate);
                if (s == null) {
                    sb.append(String.format("%-3s", 'X'));
                } else {
                    if (!sliceList.contains(s)) sliceList.add(s);
                    //int index = sliceList.indexOf(s);
                    int index = sliceIndexes.get(s);
                    sb.append(String.format("%-3s", index));
                }
            }
            sb.append("#\n");
        }
        for (int i = 0; i < pizza.cols + 1; i++) {
            sep.append(String.format("%-3s", '#'));
        }
        System.out.println(sep.toString().replace(' ', '#') + " LOOP: " + (loopCounter) + " Step: " + steps.size());
        System.out.print(sb.toString());
    }

    private List<Coordinate> generateCorners(Map<Coordinate, Slice> slices) {
        List<Coordinate> corners = new ArrayList<>();
        for (int i = 0; i < pizza.rows; i++) {
            for (int j = 0; j < pizza.cols; j++) {
                Coordinate coord = new Coordinate(i, j);
                if (slices.get(coord) != null) continue; //check if there is already a slice here

                Coordinate left = coord.left(1);
                if (left.getCol() >= 0 && slices.get(left) == null) continue;

                Coordinate up = coord.up(1);
                if (up.getRow() >= 0 && slices.get(up) == null) continue;

                //corner found
                corners.add(coord);
            }
        }
        corners.sort(coordinateComparator);
        //Collections.sort(corners);
        return corners;
    }

    private void generateOffsets() {
        for (int i = 1; i <= h; i++) {
            for (int j = 1; j <= h / i; j++) {
                CoordinateOffset offset = new CoordinateOffset(i - 1, j - 1);
                coordOffsets.add(offset);
            }
        }
    }

    private void generateSlices(Coordinate coord) {
        List<Slice> slices = new ArrayList<>();
        int rows = pizza.rows;
        int cols = pizza.cols;

        for (CoordinateOffset coordOffset : coordOffsets) {
            try {
                Slice slice = new Slice(pizza, coord, coordOffset);
                if (!isValid(slice)) continue;
                slices.add(slice);

            } catch (PizzaException e) {
                //invalid slice, no big deal
            }
        }

        /* brute force
        for (int i = coord.getRow(); i < rows; i++) {
            for (int j = coord.getCol(); j < cols; j++) {
                Coordinate nCoord = new Coordinate(i, j);
                if (coord.equals(nCoord)) continue;
                Slice slice = new Slice(pizza, new int[]{coord.getRow(), coord.getCol(), i, j});
                if (!isValid(slice)) continue;
                slices.add(slice);
            }
        }
        */
        Collections.sort(slices);
        possibleSlices.put(coord, slices);
        /*
        synchronized (System.out) {
            coordsGenerated += 1;
            System.out.print("\r" + Thread.currentThread().getName() + " " + coord.toString() + " finished " + coordsGenerated + "/" + (rows * cols));
        }
        */
    }

    private void nextStep() throws PizzaException {
        Step current = steps.get(nextStepIndex - 1);
        Coordinate coord = current.getNextCoordOption();
        List<Slice> sliceList = possibleSlices.get(coord);
        boolean back = false;
        while (sliceList.size() == 0) {
            current.addSaturatedNextStepCoordinate(coord);
            if (current.moreOptionsAvailable()) {
                coord = current.getNextCoordOption();
                sliceList = possibleSlices.get(coord);
            } else {
                //stepBack();
                back = true;
                break;
            }
        }
        if (!back) {
            steps.add(new Step(nextStepIndex++, coord));
            Collections.sort(steps);
            recheckProblemSlices();
        }
    }

    private void recheckProblemSlices() throws PizzaException {
        boolean problemFound = false;
        Map<Coordinate, List<Slice>> allSlices = new HashMap<>(possibleSlices);

        Slice problemSlice = null;

        Iterator<Coordinate> iter = allSlices.keySet().iterator();


        while (iter.hasNext()) {
            Coordinate next = iter.next();
            if (slices.get(next) != null) {
                //coord occupied remove from list
                //allSlices.remove(next);
                iter.remove();
            } else {
                allSlices.get(next).removeIf(slice -> !isValid(slice));
                if (allSlices.get(next).size() == 0) {
                    //no possible slices = biggest problem
                    if (slices.get(next) == null){
                        stepBack(); //todo this makes the programm go back too far
                        System.err.println("no possibilities");
                        return;
                    }
                } else if (allSlices.get(next).size() > 1) {
                    //not big enough of a problem
                } else {
                    //problem slice found let's fill it in
                    problemFound = true;
                    problemSlice = allSlices.get(next).get(0);
                }
            }

        }
        if (problemFound) {
            Step current = steps.get(nextStepIndex - 1);
            current.stepCoord = new Coordinate(problemSlice.getCOORDS()[0], problemSlice.getCOORDS()[1]);
            current.useSlice(problemSlice);

            //check if current step valid
            //if valid make new step and apply slice
            //if not valid undo and apply slice
        }
    }

    private void stepBack() {
        //displayPizzaSlices();
        Step current = steps.get(nextStepIndex - 1);
        Step previous = steps.get(nextStepIndex - 2);
        if (current.sliceUsed != null) {
            undoSlice(current.sliceUsed);
        }
        if (nextStepIndex - 1 != 0) {
            nextStepIndex--;
            steps.remove(nextStepIndex);//todo edited
        }
        previous.addSaturatedNextStepCoordinate(current.stepCoord);
    }

    private void undoSlice(Slice slice) {
        synchronized (slice) {
            for (int i = slice.getCOORDS()[0]; i <= slice.getCOORDS()[2]; i++) {
                for (int j = slice.getCOORDS()[1]; j <= slice.getCOORDS()[3]; j++) {
                    Coordinate coordinate = new Coordinate(i, j);
                    if (slices.get(coordinate).equals(slice)) {
                        slices.put(coordinate, null);
                        filledCells--;
                    }
                }
            }
            Step current = steps.get(nextStepIndex - 1);
            if (current != null && current.sliceUsed.equals(slice)) {
                current.sliceUsed = null;
            }
        }
        //filledCells -= slice.getSIZE();
    }

    @Override
    public void slice() throws PizzaException {
        try {
            long t0, t1, t2, time;
            t0 = System.currentTimeMillis();
            t1 = t0;
            t2 = t0;


            //Generate sliding windows
            generateOffsets();
            //Generate possible slices for each starting location (top left)
            for (int i = 0; i < pizza.rows; i++) {
                for (int j = 0; j < pizza.cols; j++) {
                    Coordinate coord = new Coordinate(i, j);
                    slices.put(coord, null);
                    threadPoolCached.execute(new SliceGenerator(coord));
                }
            }
            threadPoolCached.shutdown();
            threadPoolCached.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);

            t2 = System.currentTimeMillis();
            time = t2 - t1;
            t1 = System.currentTimeMillis();

            System.out.println("Possible slices generated " + time);
            //pizzaFrame = new PizzaFrame(pizza.rows, pizza.cols, slices, pizza);
            PizzaFrame.startGui(pizza.rows, pizza.cols, slices, pizza);

            applyProblemSlices();


            t2 = System.currentTimeMillis();
            time = t2 - t1;
            t1 = System.currentTimeMillis();

            System.out.println("Problem slices applied " + time);


            long totalLoops = 1;
            for (List<Slice> sliceList : possibleSlices.values()) {
                totalLoops *= Math.max(sliceList.size(), 1L);
            }
            System.out.println("total loops: " + totalLoops);

            if (steps.size() == 0) {
                steps.add(new Step(nextStepIndex++, new Coordinate(0, 0)));//base step (should always be the same)
                Collections.sort(steps);
            }


            t2 = System.currentTimeMillis();
            time = t2 - t1;
            t1 = System.currentTimeMillis();

            System.out.println("steps sorted " + time);

            Thread outThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (loopCounter >= 0) {
                        //System.out.print("\r       loop: " + loopCounter);
                        System.out.print("\r      steps: " + steps.size());

                        try {
                            Thread.sleep(5);
                        } catch (InterruptedException e) {
                            //ignore
                            System.out.println("ERROR");
                        }


                    }
                }
            });
            //outThread.start();//todo disable for debug

            System.out.println();
            while (filledCells < pizza.size) {
                /*
                if (loopCounter % 100000 == 0) {
                    //displayPizzaSlices(); //todo enable for debug
                }
                if (filledCells%100 ==0){
                    System.out.print("\r"+filledCells+"/"+pizza.size);
                }
                */
                //displayPizzaSlices(); //todo enable for debug
                Step currentStep = steps.get(Math.max(nextStepIndex - 1, 0));
                currentStep.processStep();
                loopCounter++;
                PizzaFrame.updatePizza(loopCounter);
            }
            System.out.println();
            System.out.println("LOOPS TAKEN: " + loopCounter);
            System.out.println();
            PizzaFrame.drawFinal();
            loopCounter = -1;
        } catch (InterruptedException e) {
            throw new PizzaException(e);
        }
    }

    @Override
    protected void spliceRows(Slice slice, boolean retry) throws PizzaException {

    }

    @Override
    protected void spliceCols(Slice slice, boolean retry) throws PizzaException {

    }

    @Override
    public List<String> outputData() {
        Set<Slice> actualSlices = new LinkedHashSet<>(slices.values());
        List<String> strings = new LinkedList<>();
        actualSlices.remove(null);
        strings.add("" + actualSlices.size());
        for (Slice actualSlice : actualSlices) {
            if (actualSlice == null) continue;
            strings.add(actualSlice.toString());
        }
        return strings;
    }

    /**
     * todo not an option (even multithreaded) with medium or larger
     */
    private class SliceGenerator implements Runnable {
        private Coordinate coord;

        public SliceGenerator(Coordinate coord) {
            this.coord = coord;
        }

        @Override
        public void run() {
            generateSlices(coord);
        }

    }

    private class Step implements Comparable<Step> {
        private final int index;
        private Coordinate stepCoord;
        private Slice sliceUsed;
        private int indexOfUsedSlice;
        private List<Coordinate> nextStepCoordsFound;
        private List<Coordinate> nextStepCoordsSaturated;

        public Step(int index, Coordinate stepCoord) {
            this.index = index;
            this.stepCoord = stepCoord;
            indexOfUsedSlice = -1;
        }

        public boolean addSaturatedNextStepCoordinate(Coordinate coord) {
            nextStepCoordsSaturated.add(coord);
            return nextStepCoordsSaturated.size() != nextStepCoordsFound.size();
        }

        @Override
        public int compareTo(Step o) {
            return Integer.compare(index, o.index);
        }

        public int getIndex() {
            return index;
        }

        public int getIndexOfUsedSlice() {
            return indexOfUsedSlice;
        }

        public Coordinate getNextCoordOption() {
            return nextStepCoordsFound.get(nextStepCoordsSaturated.size());
        }

        public Slice getSliceUsed() {
            return sliceUsed;
        }

        public Coordinate getStepCoord() {
            return stepCoord;
        }

        public boolean moreOptionsAvailable() {
            return !(nextStepCoordsFound.size() == nextStepCoordsSaturated.size());
        }

        public void processStep() throws PizzaException {
            if (sliceUsed == null) {
                useNextSlice();
            } else {
                if (moreOptionsAvailable()) {
                    nextStep();
                } else {
                    useNextSlice();
                }
            }
        }

        public void useNextSlice() throws PizzaException {
            if (possibleSlices.get(stepCoord).size() == (indexOfUsedSlice + 1)) {
                stepBack();
            } else {
                if (sliceUsed != null) {
                    undoSlice(sliceUsed);
                }
                Slice slice = possibleSlices.get(stepCoord).get(++indexOfUsedSlice);
                sliceUsed = slice;
                applySlice(slice);
                nextStepCoordsFound = generateCorners(slices);

                nextStepCoordsSaturated = new LinkedList<>();
                //recheckProblemSlices();
            }
            //displayPizzaSlices();
        }

        public void useSlice(Slice slice) throws PizzaException {
            if (sliceUsed != null) {
                undoSlice(sliceUsed);
            }
            sliceUsed = slice;
            applySlice(slice);
            nextStepCoordsFound = generateCorners(slices);

            nextStepCoordsSaturated = new LinkedList<>();
            //recheckProblemSlices();

            //displayPizzaSlices();
        }
    }
}
