package be.gato.hashcode.pizza.slicers;

import be.gato.hashcode.pizza.exception.PizzaException;
import be.gato.hashcode.pizza.pojo.CellType;
import be.gato.hashcode.pizza.pojo.Pizza;
import be.gato.hashcode.pizza.pojo.Slice;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * @author Gaston
 * @version 1.0
 * @since 4/02/2017
 */
public abstract class Slicer{
    protected final Pizza pizza;
    protected final int l;//min of each ingredient
    protected final int h;//max cells

    protected Slicer(Pizza pizza, int l, int h) throws PizzaException {
        this.pizza = pizza;
        this.l = l;
        this.h = h;
    }

    //###############################   CHECKERS
    protected boolean isValid(Slice slice) {
        return hasEnoughTomato(slice) && hasEnoughMushroom(slice) && isSizeOK(slice);
    }

    public List<Slice> filterValidSlices(List<Slice> slices){
        List<Slice> filtered = new ArrayList<>(slices);
        filtered.removeIf(s -> !isValid(s));
        return filtered;
    }

    protected boolean isProblemSlice(Slice slice) {
        return hasEnoughTomato(slice) ^ hasEnoughMushroom(slice);
    }

    protected boolean hasEnoughTomato(Slice slice) {
        return hasEnoughTomato(slice,false);
    }

    protected boolean hasEnoughTomato(Slice slice,boolean toSplice) {
        int count = countType(slice,CellType.TOMATO);
        if (toSplice) return count >= (l * 2);
        return count >= l;
    }

    protected boolean hasEnoughMushroom(Slice slice) {
        return hasEnoughMushroom(slice,false);
    }

    protected boolean hasEnoughMushroom(Slice slice,boolean toSplice) {
        int count = countType(slice, CellType.MUSHROOM);
        if (toSplice) return count >= (l * 2);
        return count >= l;
    }

    protected boolean isSizeOK(Slice slice) {
        return isSizeOK(slice, false);
    }

    protected boolean isSizeOK(Slice slice,boolean toSplice) {
        if (toSplice) return slice.getSIZE() >= (l*4);
        return (slice.getROWS() * slice.getCOLS()) <= h;
    }

    protected boolean isValidSplice(Slice s1,Slice s2) throws PizzaException {
        return hasEnoughMushroom(s1) && hasEnoughTomato(s2) && hasEnoughTomato(s1) && hasEnoughMushroom(s2);
    }

    protected int countType(Slice slice, CellType typ) {
        int count = 0;
        for (int r = slice.getCOORDS()[0]; r <= slice.getCOORDS()[2]; r++) {
            for (int c = slice.getCOORDS()[1]; c <= slice.getCOORDS()[3]; c++) {
                if (pizza.getType(r, c) == typ) count++;
            }
        }
        return count;
    }

    //#################################  PROCESSORS

    /**
     * This method starts the splicing process.
     * @throws PizzaException something went wrong
     */
    public abstract void slice() throws PizzaException;

    private void spliceRows(Slice slice) throws PizzaException {
        spliceRows(slice,false);
    }

    protected abstract void spliceRows(Slice slice,boolean retry) throws PizzaException;

    private void spliceCols(Slice slice) throws PizzaException {
        spliceCols(slice,false);
    }

    protected abstract void spliceCols(Slice slice,boolean retry) throws PizzaException;

    public abstract List<String> outputData();
}
