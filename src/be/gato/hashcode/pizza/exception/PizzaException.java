package be.gato.hashcode.pizza.exception;

/**
 * Project: hashcode 2017 example
 * Package: be.gato.hashcode.pizza
 *
 * @author Gaston
 * @version 1.0
 * @since 7/02/2017
 */
public class PizzaException extends Exception {
    public PizzaException(String message) {
        super(message);
    }

    public PizzaException(String message, Throwable cause) {
        super(message, cause);
    }

    public PizzaException(Throwable cause) {
        super(cause);
    }

    public PizzaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public PizzaException() {
    }
}
