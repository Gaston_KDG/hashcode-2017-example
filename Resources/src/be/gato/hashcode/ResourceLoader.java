package be.gato.hashcode;

import java.net.URL;

public class ResourceLoader {
    private ResourceLoader() {}
    public static final URL EXAMPLE = ResourceLoader.class.getClassLoader().getResource("example.in");
    public static final URL SMALL = ResourceLoader.class.getClassLoader().getResource("small.in");
    public static final URL MEDIUM = ResourceLoader.class.getClassLoader().getResource("medium.in");
    public static final URL BIG = ResourceLoader.class.getClassLoader().getResource("big.in");
}
